package com.example.thiago.tccapp;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;
import java.util.UUID;

public class ColetaDosDados extends AppCompatActivity {

    TextView tvVlrTemperatura, tvSituacaoArCondicionado, tvVlrEconomizado, tvVlrGasto;
    Handler bluetoothIn;

    final int handlerState = 0;                        //used to identify handler message
    private BluetoothAdapter btAdapter = null;
    private BluetoothSocket btSocket = null;
    private StringBuilder recDataString = new StringBuilder();

    private ConnectedThread mConnectedThread;

    // SPP UUID service - this should work for most devices
    private static final UUID BTMODULEUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    // String for MAC address
    private static String address;
    private BluetoothDevice device;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coleta_dos_dados);

        tvVlrTemperatura = (TextView) findViewById(R.id.tvVlrTemperatura);
        tvSituacaoArCondicionado = (TextView) findViewById(R.id.tvSituacaoArCondicionado);
        tvVlrEconomizado = (TextView) findViewById(R.id.tvValorEconomizado);
        tvVlrGasto = (TextView) findViewById(R.id.tvVlrGasto);

        try {
            bluetoothIn = new Handler() {
                public void handleMessage(android.os.Message msg) {
                    if (msg.what == handlerState) {
                        String readMessage = (String) msg.obj;
                        recDataString.append(readMessage);
                        int endOfLineIndex = recDataString.indexOf("~");
                        if (endOfLineIndex > 0) {
                            String dataInPrint = recDataString.substring(0, endOfLineIndex);

                            int dataLength = dataInPrint.length();

                            String temperature = dataInPrint.substring(1, 3);
                            Integer temperatura = Integer.parseInt(temperature.toString());
                            tvVlrTemperatura.setText(temperature.toString());

                            try {
                                Logs log = new Logs(getBaseContext());
                                LogsDb logsDb = new LogsDb((getBaseContext()));
                                Economia economia = new Economia(getBaseContext());
                                economia.carregarCalculosDeGastos();
                                if (temperatura > 23) {
                                    if (!log.existeAlgumLogRegistrado()) {
                                        log.registrarLogs("Ar Condicionado", "Ligado");
                                    } else {
                                        if (log.oArCondicionadoAindaPermaneceDesligado()) {
                                            Map ultimoLog = log.obterUltimoLog();
                                            String idUltimoLog = (String)ultimoLog.get(logsDb.id);
                                            log.atualizarLog(idUltimoLog);
                                            log.registrarLogs("Ar Condicionado", "Ligado");
                                        }
                                    }
                                    tvSituacaoArCondicionado.setTextColor(Color.BLUE);
                                    tvSituacaoArCondicionado.setText("Ligado");
                                } else {
                                    if (!log.existeAlgumLogRegistrado()) {
                                        log.registrarLogs("Ar Condicionado", "Desligado");
                                    } else {
                                        if (log.oArCondicionadoAindaPermaneceLigado()) {
                                            Map ultimoLog = log.obterUltimoLog();
                                            String idUltimoLog = (String)ultimoLog.get(logsDb.id);
                                            log.atualizarLog(idUltimoLog);
                                            log.registrarLogs("Ar Condicionado", "Desligado");
                                        }
                                    }
                                    tvSituacaoArCondicionado.setTextColor(Color.BLACK);
                                    tvSituacaoArCondicionado.setText("Desligado");
                                }
                                String vlrEconomizado = economia.obterVlrEconomizado();
                                String vlrGasto = economia.obterVlrGasto();
                                tvVlrEconomizado.setText(vlrEconomizado);
                                tvVlrGasto.setText(vlrGasto);
                            } catch (Exception e) {
                                tvSituacaoArCondicionado.setText(e.getMessage());
                            }

                            recDataString.delete(0, recDataString.length());
                            dataInPrint = " ";
                        }
                    }
                }
            };

            btAdapter = BluetoothAdapter.getDefaultAdapter();
            checkBTState();
        } catch (Exception e) {
            tvSituacaoArCondicionado.setText(e.getMessage());
        }
    }

    private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException {
        // cria uma conexao segura com o bluetooth utilizando o UUID
        return  device.createRfcommSocketToServiceRecord(BTMODULEUUID);
    }

    @Override
    public void onResume() {
        super.onResume();

        Intent intent = getIntent();

        // Obtem o MAC adrress do dispositivo pareado escolhido
        address = intent.getStringExtra(MainActivity.EXTRA_ADDRESS);

        // cria o dispositivo e define o MAC address utilizado
        BluetoothDevice device = btAdapter.getRemoteDevice(address);

        try {
            btSocket = createBluetoothSocket(device);
        } catch (IOException e) {
            Toast.makeText(getBaseContext(), "Socket creation failed", Toast.LENGTH_LONG).show();
        }
        //Estabelece conexao via socket
        try {
            btSocket.connect();
        } catch (IOException e) {
            try  {
                btSocket.close();
            } catch (IOException e2) {
                // tratamento do erro aqui
            }
        }
        mConnectedThread = new ConnectedThread(btSocket);
        mConnectedThread.start();

        // send a character when resuming.beginning transmission to check device is connected
        //If it is not an exception will be thrown in the write method and finish() will be called
        mConnectedThread.write("x");
    }

    @Override
    public void onPause()
    {
        super.onPause();
        try {
            //Nao deixa os sockets abertos quando deixar a activity
            btSocket.close();
        } catch (IOException e2) {
            try {
                btSocket = createBluetoothSocket(device);
            } catch (IOException e) {
                Toast.makeText(getBaseContext(), "Socket creation failed", Toast.LENGTH_LONG).show();
            }
            //Estabelece conexao via socket
            try {
                btSocket.connect();
            } catch (IOException e) {
                try  {
                    btSocket.close();
                } catch (IOException e3) {
                    // tratamento do erro aqui
                }
            }
            mConnectedThread = new ConnectedThread(btSocket);
            mConnectedThread.start();

            //I send a character when resuming.beginning transmission to check device is connected
            //If it is not an exception will be thrown in the write method and finish() will be called
            mConnectedThread.write("x");
        }
    }

    //Checks that the Android device Bluetooth is available and prompts to be turned on if off
    private void checkBTState() {

        if(btAdapter==null) {
            Toast.makeText(getBaseContext(), "Dispositivo não tem suporte para Bluetooth", Toast.LENGTH_LONG).show();
        } else {
            if (btAdapter.isEnabled()) {
            } else {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, 1);
            }
        }
    }

    //classe para conexao da thread
    private class ConnectedThread extends Thread {
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        // criacao da thread conectada
        public ConnectedThread(BluetoothSocket socket) {
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) { }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            byte[] buffer = new byte[256];
            int bytes;

            while (true) {
                try {
                    bytes = mmInStream.read(buffer); // lear os bytes do buffer de entrada
                    String readMessage = new String(buffer, 0, bytes);
                    // envia os bytes adquiridos para a UI Activity via hadler
                    bluetoothIn.obtainMessage(handlerState, bytes, -1, readMessage).sendToTarget();
                } catch (IOException e) {
                    break;
                }
            }
        }

        public void write(String input) {
            byte[] msgBuffer = input.getBytes(); // convert string para bytes
            try {
                mmOutStream.write(msgBuffer); // grava os bytes na conexao bluetooth via outstream
            } catch (IOException e) {
                Toast.makeText(getBaseContext(), "Falha na conexão", Toast.LENGTH_LONG).show();
                finish();

            }
        }
    }
}
