package com.example.thiago.tccapp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by thiago on 21/03/17.
 */

public class LogsDb extends SQLiteOpenHelper {

    public static final String bancoDeDados = "tcc.db";
    public static final String tabela = "logs";
    public static final String id = "_id";
    public static final String componente = "componente";
    public static final String acao = "acao";
    public static final String data_inicio = "data_inicio";
    public static final String data_fim = "data_fim";

    public LogsDb(Context context) {
        super(context, bancoDeDados , null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE "+tabela+ "( "
                + id + " integer primary key autoincrement, "
                + componente + " text,"
                + acao + " text,"
                + data_inicio + " datetime,"
                + data_fim + " datetime"
                +")";

        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + tabela);
        onCreate(db);
    }
}
