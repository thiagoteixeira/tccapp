package com.example.thiago.tccapp;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_ENABLE_BT = 1;
    public static String EXTRA_ADDRESS = "";

    private AdapterView.OnItemClickListener myListClickListener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> av, View v, int arg2, long arg3) {

            TextView msg = (TextView) findViewById(R.id.tvMsgAlerta);
            msg.setText("");

            // Get the device MAC address, which is the last 17 chars in the View
            String info = ((TextView) v).getText().toString();
            String address = info.substring(info.length() - 17);

            // Make an intent to start next activity while taking an extra which is the MAC address.
            Intent i = new Intent(MainActivity.this, ColetaDosDados.class);
            i.putExtra(EXTRA_ADDRESS, address);

            startActivity(i);

            }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void iniciarColetaDosDados(View view)
    {
        TextView msg = (TextView) findViewById(R.id.tvMsgAlerta);

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            msg.setText("Este dispositivo não tem suporte para bluetooth");
        } else {
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }

            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

            ArrayList listaDeDispositivos = new ArrayList();

            if (pairedDevices.size() > 0) {
                // lista dos dispositivos pareados.
                for (BluetoothDevice device : pairedDevices) {
                    String deviceName = device.getName();
                    String deviceAddress = device.getAddress(); // MAC address
                    listaDeDispositivos.add(deviceName + "\n" + deviceAddress);
                }

                ListView dispositivosPareadosListView = (ListView) findViewById(R.id.lvListaDispositivos);
                ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, listaDeDispositivos);
                dispositivosPareadosListView.setAdapter(adapter);
                dispositivosPareadosListView.setOnItemClickListener(myListClickListener);
            }
        }
    }

    public void verLogs(View view)
    {
        Intent i = new Intent(MainActivity.this, LogsActivity.class);
        startActivity(i);
    }
}
