package com.example.thiago.tccapp;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by thiago on 23/03/17.
 */

public class Economia {

    private Double vlrEconomizado;
    private Double vlrGasto;
    private double  taxaCemig = 0.54622;
    private double consumoKwhArCondicionado = 2.68;
    private SQLiteDatabase sqlLiteDb;
    private LogsDb logsdb;

    public Economia(Context context)
    {
        logsdb = new LogsDb(context);
    }

    public void carregarCalculosDeGastos()
    {
        String calculoGasto = "ROUND(((SUM(strftime('%s',data_fim) - strftime('%s',data_inicio)) * "+consumoKwhArCondicionado+") / 3600) * "+taxaCemig+", 2) AS vlr_total";
        String[] campos ={calculoGasto, "acao"};
        String[] where = {"data_fim IS NOT NULL"};
        Cursor cursor;

        sqlLiteDb = logsdb.getReadableDatabase();
        cursor = sqlLiteDb.query(logsdb.tabela, campos, "data_fim is not null", null, "acao", null, null);

        //sqlLiteDb = logsdb.getReadableDatabase();
        ///cursor = sqlLiteDb.rawQuery(sql, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();

            while (cursor.isAfterLast() == false) {
                String acao = (String) cursor.getString(cursor.getColumnIndex(logsdb.acao));

                if (acao.equals("Desligado")) {
                    Double vlrEconomizado = (Double) cursor.getDouble(cursor.getColumnIndex("vlr_total"));
                    definirVlrEconomizado(vlrEconomizado);
                } else {
                    Double vlrGasto = (Double) cursor.getDouble(cursor.getColumnIndex("vlr_total"));
                    definirVlrGasto(vlrGasto);
                }
                cursor.moveToNext();
            }
        }
        this.sqlLiteDb.close();
    }

    private void definirVlrEconomizado(Double vlrEconomizado)
    {
        this.vlrEconomizado = vlrEconomizado;
    }

    public String obterVlrEconomizado()
    {
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.getDefault());
        String valorEconomizadoFormatado = format.format(vlrEconomizado);
        return valorEconomizadoFormatado;
    }

    private void definirVlrGasto(Double vlrGasto)
    {
        this.vlrGasto = vlrGasto;
    }

    public String obterVlrGasto()
    {
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.getDefault());
        String valorGastoFormatado = format.format(vlrGasto);
        return valorGastoFormatado;
    }

}
