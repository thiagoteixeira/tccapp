package com.example.thiago.tccapp;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

public class LogsActivity extends AppCompatActivity {

    private ListView lvLogs;
    private Cursor cursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logs);

        lvLogs = (ListView) findViewById(R.id.lvLogs);

        Logs log = new Logs(getBaseContext());
        LogsDb logsDb = new LogsDb(getBaseContext());

        try {
            /*
            Random gerador = new Random();
            Integer temperatura = gerador.nextInt(11) + 20;

            Toast.makeText(getBaseContext(), temperatura.toString(), Toast.LENGTH_LONG).show();

            if (temperatura > 25) {
                if (!log.existeAlgumLogRegistrado()) {
                    log.registrarLogs("Ar Condicionado", "Ligado");
                } else {
                    if (log.oArCondicionadoAindaPermaneceDesligado()) {
                        Map ultimoLog = log.obterUltimoLog();
                        String idUltimoLog = (String)ultimoLog.get(logsDb.id);
                        log.atualizarLog(idUltimoLog);
                        log.registrarLogs("Ar Condicionado", "Ligado");
                    }
                }
            } else {
                if (!log.existeAlgumLogRegistrado()) {
                    log.registrarLogs("Ar Condicionado", "Desligado");
                } else {
                    if (log.oArCondicionadoAindaPermaneceLigado()) {
                        Map ultimoLog = log.obterUltimoLog();
                        String idUltimoLog = (String)ultimoLog.get(logsDb.id);
                        log.atualizarLog(idUltimoLog);
                        log.registrarLogs("Ar Condicionado", "Desligado");
                    }
                }
            } */

                Cursor cursor = log.obterTodosLogs();

                String[] nomeDosCampos = new String[] {LogsDb.componente, LogsDb.acao, LogsDb.data_inicio, LogsDb.data_fim};
                int[] idViews = new int[] {R.id.tvCampoComponente, R.id.tvCampoAcao, R.id.tvCampoDataInicio, R.id.tvCampoDataFim};

                SimpleCursorAdapter adaptador = new SimpleCursorAdapter(
                        getBaseContext(),
                        R.layout.listagem_logs_personalizada,
                        cursor,
                        nomeDosCampos,
                        idViews,
                        0
                );

                ListView lista = (ListView)findViewById(R.id.lvLogs);
                lista.setAdapter(adaptador);

        } catch (Exception e) {
            TextView lvLogtitulo = (TextView) findViewById(R.id.textView2);
            lvLogtitulo.setText(e.getMessage() + " " + e.getStackTrace());
        }
    }
}
