package com.example.thiago.tccapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by thiago on 20/03/17.
 */



public class Logs {

    private SQLiteDatabase sqlLiteDb;
    private LogsDb logsdb;
    private String dataAtual;
    private Map logMap;

    public Logs(Context context)
    {
        logsdb = new LogsDb(context);
    }

    public String obterDataAtual()
    {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String data = sdf.format(c.getTime());
        return data;
    }

    public void registrarLogs(String componente, String acao)
    {
        ContentValues valores;
        sqlLiteDb = logsdb.getWritableDatabase();
        valores = new ContentValues();
        valores.put(logsdb.componente, componente);
        valores.put(logsdb.acao, acao);
        valores.put(logsdb.data_inicio, obterDataAtual());

        Long resultado = sqlLiteDb.insert(logsdb.tabela, null, valores);
        sqlLiteDb.close();
    }

    public Cursor obterTodosLogs()
    {
        Cursor cursor;
        String[] campos = {logsdb.id, logsdb.componente, logsdb.acao, logsdb.data_inicio, logsdb.data_fim};
        sqlLiteDb = logsdb.getReadableDatabase();
        cursor = sqlLiteDb.query(logsdb.tabela, campos, null, null, null, null, null, "20");

        if (cursor != null) {
            cursor.moveToFirst();
        }
        this.sqlLiteDb.close();
        return cursor;
    }

    public boolean existeAlgumLogRegistrado()
    {
        Map log = obterUltimoLog();
        return (log.isEmpty()) ? false : true;
    }

    public boolean oArCondicionadoAindaPermaneceLigado()
    {
        Map log = obterUltimoLog();

        String statusArCondicionado = (String)log.get(logsdb.acao);
        String data_fim = (String)log.get("data_fim");
        return statusArCondicionado.equals("Ligado");
    }

    public boolean oArCondicionadoAindaPermaneceDesligado()
    {
        Map log = obterUltimoLog();

        String statusArCondicionado = (String)log.get(logsdb.acao);
        String data_fim = (String)log.get("data_fim");
        return statusArCondicionado.equals("Desligado");
    }

    public Map obterUltimoLog()
    {
        Cursor cursor;
        String[] campos = {logsdb.id, logsdb.componente, logsdb.acao, logsdb.data_inicio, logsdb.data_fim};
        sqlLiteDb = logsdb.getReadableDatabase();
        cursor = sqlLiteDb.query(logsdb.tabela, campos, null, null, null, null, logsdb.id + " DESC", "1");

        HashMap<String, String> logMap = new HashMap<String, String>();
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            logMap.put(logsdb.id, cursor.getString(cursor.getColumnIndex(logsdb.id)));
            logMap.put(logsdb.componente, cursor.getString(cursor.getColumnIndex(logsdb.componente)));
            logMap.put(logsdb.acao, cursor.getString(cursor.getColumnIndex(logsdb.acao)));
            logMap.put(logsdb.data_inicio, cursor.getString(cursor.getColumnIndex(logsdb.data_inicio)));
            logMap.put(logsdb.data_fim, cursor.getString(cursor.getColumnIndex(logsdb.data_fim)));
        }
        this.sqlLiteDb.close();
        return logMap;
    }

    public HashMap<String, String> obterUltimoLogDesligado()
    {
        Cursor cursor;
        String[] campos = {logsdb.id, logsdb.componente, logsdb.acao, logsdb.data_inicio, logsdb.data_fim};
        sqlLiteDb = logsdb.getReadableDatabase();
        cursor = sqlLiteDb.query(logsdb.tabela, campos, logsdb.acao + "=Desligado", null, null, null, logsdb.id + " DESC", "1");

        HashMap<String, String> logMap = new HashMap<String, String>();
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            logMap.put(logsdb.id, cursor.getString(cursor.getColumnIndex(logsdb.id)));
            logMap.put(logsdb.componente, cursor.getString(cursor.getColumnIndex(logsdb.componente)));
            logMap.put(logsdb.acao, cursor.getString(cursor.getColumnIndex(logsdb.acao)));
            logMap.put(logsdb.data_inicio, cursor.getString(cursor.getColumnIndex(logsdb.data_inicio)));
            logMap.put(logsdb.data_fim, cursor.getString(cursor.getColumnIndex(logsdb.data_fim)));
        } else {
            logMap.put(logsdb.acao, "");
            logMap.put(logsdb.data_fim, "");
        }
        this.sqlLiteDb.close();
        return logMap;
    }

    public void atualizarLog(String id)
    {
        ContentValues valores;
        valores = new ContentValues();
        valores.put(logsdb.data_fim, obterDataAtual());
        sqlLiteDb = logsdb.getWritableDatabase();
        String where = logsdb.id + "=" + id;
        sqlLiteDb.update(logsdb.tabela, valores, where, null);
    }

}
