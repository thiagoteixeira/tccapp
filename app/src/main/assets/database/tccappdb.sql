--
-- File generated with SQLiteStudio v3.1.1 on dom mar 19 22:27:25 2017
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: logs
CREATE TABLE logs (log_id INTEGER PRIMARY KEY AUTOINCREMENT, componente VARCHAR (50), acao VARCHAR (10), data_inicio DATETIME, data_fim DATETIME);
INSERT INTO logs (log_id, componente, acao, data_inicio, data_fim) VALUES (1, 'ar-condicionado', 'on', '2017-03-19 22:00:00', '2017-03-19 23:00:00');
INSERT INTO logs (log_id, componente, acao, data_inicio, data_fim) VALUES (2, 'ar-condicionado', 'off', '2017-03-19 21:00:00', '2017-03-19 22:00:00');

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
